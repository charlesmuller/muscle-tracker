# muscle-tracker

**Requisitos:**

- Node.js (mínimo): ^14.17.0
- npm (mínimo): ^7.0.0

Recomendamos o uso do nvm para gerenciar as versões do Node.js e npm.

**Construção e Execução do Contêiner:**
- Certifique-se de ter o Docker instalado.
- Use o seguinte comando para construir e executar o contêiner:

  ```bash
  docker-compose up -d

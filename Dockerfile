FROM node:16

WORKDIR /usr/src/

COPY package*.json ./

RUN mkdir -p /root/.npm/_cacache/tmp && chown -R 1364806413:1364800513 /root/.npm

RUN npm install

COPY . .

EXPOSE 3000

CMD ["node", "src/index.js"]


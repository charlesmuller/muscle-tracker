const { Sequelize } = require('sequelize');

const sequelize = new Sequelize('muscletracker-db', 'muscletracker', '135078', {
    host: 'localhost',
    dialect: 'mysql',
    define: {
        timestamps: false,
    },
});

module.exports = { sequelize };

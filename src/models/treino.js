// models/Treino.js
const { DataTypes } = require('sequelize');
const { sequelize } = require('../database');

const Treino = sequelize.define('Treino', {
    treinoEspecifico: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    exercicios: {
        type: DataTypes.STRING,
        allowNull: false,
    },
});

module.exports = Treino;

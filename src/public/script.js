function adicionarTreino() {
    console.log('Adicionar treino');
}

function adicionarMedida() {
    console.log('Adicionar medida');
}

function editarTreino(treinoId) {
    console.log('Editar treino:', treinoId);
}

function removerTreino(treinoId) {
    console.log('Remover treino:', treinoId);
}

$(document).ready(function () {
    $("#menuButton").click(function () {
      $(this).toggleClass("active");
      $("#navbarNav").toggleClass("show");

    });
  });

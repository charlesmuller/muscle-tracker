const express = require('express');
const homeRouter = require('./home');
const metricsRouter = require('./metrics');
const targetsRouter = require('./targets');
const treinoController = require('../controllers/treinoController');

const router = express.Router();

router.use('/', homeRouter);
router.use('/metrics', metricsRouter);
router.use('/targets', targetsRouter);

router.post('/adicionarTreino', treinoController.adicionarTreino);

module.exports = router;


const express = require('express');
const router = express.Router();

const medidas = [
  { id: "1", nome: 'Largura do peito', medida: "100cm", diaMedicao: "01-01-2024"},
  { id: "2", nome: 'Linha de cintura', medida: '100cm', diaMedicao: "15-01-2024"},
]

router.get('/', (req, res) => {
  res.render('metrics', { title: 'Medidas', medidas});
});

module.exports = router;
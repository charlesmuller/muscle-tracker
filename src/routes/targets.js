const express = require('express');
const router = express.Router();

router.get('/', (req, res) => {
  res.render('targets', { title: 'Metas' });
});

module.exports = router;
const express = require('express');
const router = express.Router();

const treinos = [
  { id: "1", nome: 'Treino A', descricao: 'Treino de peito e biceps' },
  { id: "2", nome: 'Treino B', descricao: 'Treino de ombros e triceps'},
]

router.get('/', (req, res) => {
  res.render('home', { title: 'Treinos', treinos});
});

module.exports = router;

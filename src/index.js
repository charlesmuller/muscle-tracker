const express = require('express');
const path = require('path');
const routes = require('./routes/routes');
const ejs = require('ejs');

const app = express();

app.use(express.urlencoded({ extended: true }));

app.set('view engine', 'ejs'); 
app.set('views', path.join(__dirname, 'views'));

// Middleware de arquivos estáticos
app.use(express.static(path.join(__dirname, 'public')));

app.use(routes);

ejs.localsName = 'locals';
app.set('view options', { layout: 'layouts/layout' });

const PORT = process.env.PORT || 3001;
app.listen(PORT, () => {
  console.log(`Servidor ouvindo na porta: ${PORT}`);
});
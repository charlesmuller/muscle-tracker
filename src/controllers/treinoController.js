const adicionarTreino = (req, res) => {
    const treinoEspecifico = req.body.treinoEspecifico;
    const exercicios = req.body.exercicios;

    if (!treinoEspecifico || !exercicios) {
        return res.status(400).send({ message: "Campos obrigatórios não foram preenchidos." });
    }

    const novoTreino = new Treino({
        treinoEspecifico,
        exercicios,
    });

    novoTreino.save()
        .then(() => {
            res.send('Treino adicionado com sucesso!');
        })
        .catch((err) => {
            console.error('Erro ao salvar o treino:', err);
            res.status(500).send('Erro interno do servidor.');
        });
};

module.exports = { adicionarTreino };
